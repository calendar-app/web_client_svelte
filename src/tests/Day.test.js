import '@testing-library/jest-dom'
import { render } from '@testing-library/svelte'

import Day from '../components/Day.svelte'
import { capitalize } from '../utils'

it('Renders props correctly', () => {

  const dtf = new Intl.DateTimeFormat('it', {weekday: 'long', month: 'long', day: 'numeric'})

  const date = new Date(2020, 0, 0)
  const events = [{
    timeStart: '13.00',
    timeEnd: '20.00',
    professor: 'A REALLY LONG NAME',
    room: 'R 20',
  }]

  const { getByTestId } = render(Day,{
    date,
    events,
  })

  expect(getByTestId('day-date')).toHaveTextContent(capitalize(dtf.format(date)))
  expect(getByTestId('day-event-module')).toBeInTheDocument()
  expect(getByTestId('day-event-time')).toBeInTheDocument()
  expect(getByTestId('day-event-professor')).toBeInTheDocument()
  expect(getByTestId('day-event-room')).toBeInTheDocument()
})

it('Loading state', async() => {

  const date = new Date(2020, 11, 4)
  const events = [{
    timeStart: '13.00',
    timeEnd: '20.00',
    professor: 'A REALLY LONG NAME',
    room: 'R 20',
  }]

  const { queryByTestId } = render(Day,{
    loading: true,
    date,
    events,
  })

  expect(queryByTestId('day-date')).not.toBeInTheDocument()
  expect(queryByTestId('day-event-module')).not.toBeInTheDocument()
  expect(queryByTestId('day-event-time')).not.toBeInTheDocument()
  expect(queryByTestId('day-event-professor')).not.toBeInTheDocument()
  expect(queryByTestId('day-event-room')).not.toBeInTheDocument()

  expect(queryByTestId('day-date-loading')).toBeInTheDocument()
  expect(queryByTestId('day-module-loading')).toBeInTheDocument()
  expect(queryByTestId('day-time-room-loading')).toBeInTheDocument()
  expect(queryByTestId('day-professor-loading')).toBeInTheDocument()
})
