import '@testing-library/jest-dom'
import { render } from '@testing-library/svelte'

import nock from 'nock'
import {apiUrl, getCourseEventsEndpoint} from '../api'

import {sleep} from '../utils'

import Content from '../components/Content.svelte'

it('Renders selected course', async () => {

    const selectedCourse = 'IoT-A2'
    const responseBody = []

    nock(apiUrl)
      .get(getCourseEventsEndpoint)
      .reply(200, responseBody)

    const { getByTestId, queryByText, component } = render(Content, {selectedCourse})

    await sleep(100)

    // TODO: Complete test
    // expect(content).toEqual(responseBody[0])
})

it('Renders course selection', async () => {

  const selectedCourse = ''

  const { queryByTestId, component } = render(Content, {selectedCourse})

  expect(queryByTestId('content-course')).not.toBeInTheDocument()
  expect(queryByTestId('content-select-course')).toBeInTheDocument()
})
