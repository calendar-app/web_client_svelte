import { isToday, isTomorrow, capitalize } from '../utils'

describe('isToday()', () => {
  it('Should return true (today)', async () => {

    const date = new Date()

    expect(isToday(date)).toBe(true)
  })

  it('Should return false (tomorrow)', async () => {

    const date = new Date()
    date.setDate(date.getDate() + 1)

    expect(isToday(date)).toBe(false)
  })

  it('Should return false (yesterday)', async () => {

    const date = new Date()
    date.setDate(date.getDate() - 1)

    expect(isToday(date)).toBe(false)
  })

  it('Should return false (random)', async () => {

    const date = new Date()
    date.setDate(date.getDate() - Math.floor(Math.random() * 365  + 1))

    expect(isToday(date)).toBe(false)
  })
})

describe('isTomorrow()', () => {
  it('Should return true (tomorrow)', async () => {
    const date = new Date()
    date.setDate(date.getDate() + 1)

    expect(isTomorrow(date)).toBe(true)
  })

  it('Should return false (today)', async () => {

    const date = new Date()

    expect(isTomorrow(date)).toBe(false)
  })

  it('Should return false (yesterday)', async () => {

    const date = new Date()
    date.setDate(date.getDate() - 1)

    expect(isTomorrow(date)).toBe(false)
  })

  it('Should return false (day after tomorrow)', async () => {
    const date = new Date()
    date.setDate(date.getDate() + 2)

    expect(isTomorrow(date)).toBe(false)
  })

  it('Should return false (random)', async () => {

    const date = new Date()
    date.setDate(date.getDate() - Math.floor(Math.random() * 365  + 1))

    expect(isTomorrow(date)).toBe(false)
  })
})

describe('capitalize()', () => {
  it('String already capitalized', async () => {

    const str = 'Capitalized'

    expect(capitalize(str)).toEqual('Capitalized')
  })

  it('Lowercase string', async () => {

    const str = 'capitalized'

    expect(capitalize(str)).toEqual('Capitalized')
  })

  it('Uppercase string', async () => {

    const str = 'CAPITALIZED'

    expect(capitalize(str)).toEqual('Capitalized')
  })

  it('Mixed Case string', async () => {

    const str = 'CAPITalized'

    expect(capitalize(str)).toEqual('Capitalized')
  })

  it('Mixed Case string 2', async () => {

    const str = 'capitALIZED'

    expect(capitalize(str)).toEqual('Capitalized')
  })

  it('String with spaces', async () => {

    const str = 'Ca pi ta li zed'

    expect(capitalize(str)).toEqual('Ca Pi Ta Li Zed')
  })

  it('Lowercase string with spaces', async () => {

    const str = 'ca pi ta li zed'

    expect(capitalize(str)).toEqual('Ca Pi Ta Li Zed')
  })

  it('Uppercase string with spaces', async () => {

    const str = 'CA PI TA LI ZED'

    expect(capitalize(str)).toEqual('Ca Pi Ta Li Zed')
  })

  it('Mixed case string with spaces', async () => {

    const str = 'cA pI Ta LI zed'

    expect(capitalize(str)).toEqual('Ca Pi Ta Li Zed')
  })

  it('String with trailing spaces', async () => {

    const str = ' capitalized '

    expect(capitalize(str)).toEqual(' Capitalized ')
  })
})

describe('sleep()', () => {})
