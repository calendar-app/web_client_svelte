import '@testing-library/jest-dom'
import { render, fireEvent } from '@testing-library/svelte'
import {sleep} from '../utils'

import Messages from '../components/Messages.svelte'

it('OnConfigReceived fires', async () => {

  const { getByTestId, component } = render(Messages)

  let received = false
  component.$on('configReceived', event => received = true)

  window.postMessage({type: 'config', data: {}}, '*')
  await sleep(100)

  expect(received).toEqual(true)
})
