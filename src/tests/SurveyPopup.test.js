import '@testing-library/jest-dom'
import { render, fireEvent } from '@testing-library/svelte'

import SurveyPopup from '../components/SurveyPopup.svelte'

it('Should close the popup', async () => {

  const { getByTestId, queryByTestId } = render(SurveyPopup)

  expect(getByTestId('survey-popup')).toBeInTheDocument()

  await fireEvent.click(getByTestId('survey-popup-close'))

  expect(queryByTestId('survey-popup')).toBeNull()
})
