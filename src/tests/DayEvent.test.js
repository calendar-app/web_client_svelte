import '@testing-library/jest-dom'
import { render } from '@testing-library/svelte'

import DayEvent from '../components/DayEvent.svelte'

it('Renders props correctly', () => {

  const module = 'Module Name'
  const timeStart = '13.00'
  const timeEnd = '20.00'
  const professor = 'A REALLY LONG NAME'
  const room = 'R 20'

  const { getByTestId } = render(DayEvent,{
    module,
    timeStart,
    timeEnd,
    professor,
    room
  })

  expect(getByTestId('day-event-module')).toHaveTextContent(module)
  expect(getByTestId('day-event-time')).toHaveTextContent(`${timeStart} - ${timeEnd}`)
  expect(getByTestId('day-event-professor')).toHaveTextContent (new RegExp(professor,"i"))
  expect(getByTestId('day-event-room')).toHaveTextContent(room)
})
