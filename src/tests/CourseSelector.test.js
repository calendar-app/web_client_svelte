import '@testing-library/jest-dom'
import { fireEvent, render } from '@testing-library/svelte'

import nock from 'nock'
import {apiUrl, getCoursesEndpoint} from '../api'

import {sleep} from '../utils'

import CourseSelector from '../components/CourseSelector.svelte'

it('Shows loading message', async () => {

  nock(apiUrl)
    .get(getCoursesEndpoint)
    .delay(500)

  const { getByTestId } = render(CourseSelector)

  expect(getByTestId('course-selector-loading-message')).toBeInTheDocument()

})

describe('Courses tests', () => {

  it('Clicking works', async () => {

    const responseBody = [ "IoT-A2", "DaM-A2", "FEX-A1", "IIOT-A1" ]

    nock(apiUrl)
      .get(getCoursesEndpoint)
      .reply(200, responseBody)

    const { queryByTestId, queryByText } = render(CourseSelector)

    await sleep(100)

    expect(queryByTestId('course-selector-select-course')).toBeInTheDocument()

    await fireEvent.click(queryByTestId('course-selector-courses-wrapper'))

    expect(queryByTestId('course-selector-select-course')).not.toBeInTheDocument()

    let regexp = new RegExp(responseBody[0], "i")
    expect(queryByText(regexp)).toBeInTheDocument()

    await fireEvent.click(queryByText(regexp))

    expect(queryByTestId('course-selector-selected-course')).toBeInTheDocument()
    expect(regexp.test(queryByTestId('course-selector-selected-course').textContent)).toBeTruthy()
  })

  it('Shows all courses', async () => {

    const responseBody = [ "IoT-A2", "DaM-A2", "FEX-A1", "IIOT-A1" ]

    nock(apiUrl)
      .get(getCoursesEndpoint)
      .reply(200, responseBody)

    const { getByTestId } = render(CourseSelector)

    await sleep(100)

    await fireEvent.click(getByTestId('course-selector-courses-wrapper'))

    responseBody.forEach(course => {
      let regexp = new RegExp(course,"i")
      expect(regexp.test(getByTestId('course-selector-courses-wrapper').textContent)).toBeTruthy()
    })

  })

  it('Fires Events', async () => {

    const responseBody = [ "IoT-A2", "DaM-A2", "FEX-A1", "IIOT-A1" ]
    let course = ''

    nock(apiUrl)
      .get(getCoursesEndpoint)
      .reply(200, responseBody)

    const { getByTestId, queryByText, component } = render(CourseSelector)

    component.$on('courseChanged', event => course = event.detail)

    await sleep(100)

    await fireEvent.click(getByTestId('course-selector-courses-wrapper'))

    let regexp = new RegExp(responseBody[0], "i")

    await fireEvent.click(queryByText(regexp))

    expect(course).toEqual(responseBody[0])

  })

})

it('Shows error message', async () => {

  nock(apiUrl)
    .get(getCoursesEndpoint)
    .reply(400)

  const { getByTestId } = render(CourseSelector)

  await sleep(100)

  expect(getByTestId('course-selector-error-message')).toBeInTheDocument()

})
