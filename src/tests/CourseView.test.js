import '@testing-library/jest-dom'
import { render } from '@testing-library/svelte'

import nock from 'nock'
import {apiUrl, getCourseEventsEndpoint} from '../api'

import {sleep} from '../utils'

import CourseView from '../components/CourseView.svelte'

it('Renders selected course', async () => {

    const selectedCourse = 'testCourse'
    const responseBody = [{
      days: [
        {
          date:       new Date().toISOString(),
          module:     'Module',
          professor:  'Professor name',
          timeStart:  '8.00',
          timeEnd:    '10.00',
          room:       'R2',
        }
      ]
    }]

    nock(apiUrl)
      .get(new RegExp(getCourseEventsEndpoint))
      .reply(200, responseBody)

    const { getByTestId, getAllByTestId, component } = render(CourseView, {selectedCourse})

    await sleep(100)

    expect(getByTestId('course-view-events-wrapper')).toBeInTheDocument()


    expect(getAllByTestId('day-date').length).toBeGreaterThan(0)
    getAllByTestId('day-date').forEach(element => expect(element).toBeInTheDocument())

    expect(getAllByTestId('day-event-module').length).toEqual(responseBody[0].days.length)
    expect(getAllByTestId('day-event-time').length).toEqual(responseBody[0].days.length)
    expect(getAllByTestId('day-event-professor').length).toEqual(responseBody[0].days.length)
    expect(getAllByTestId('day-event-room').length).toEqual(responseBody[0].days.length)

    getAllByTestId('day-event-module').forEach(element => expect(element).toBeInTheDocument())
    getAllByTestId('day-event-time').forEach(element => expect(element).toBeInTheDocument())
    getAllByTestId('day-event-professor').forEach(element => expect(element).toBeInTheDocument())
    getAllByTestId('day-event-room').forEach(element => expect(element).toBeInTheDocument())
})

it('Renders loading animations', async () => {

  const selectedCourse = 'testCourse'

  nock(apiUrl)
    .get(new RegExp(getCourseEventsEndpoint))
    .delay(500)
    .reply(200)

  const { queryByTestId, getAllByTestId, component } = render(CourseView, {selectedCourse})

  expect(queryByTestId('course-view-events-wrapper')).not.toBeInTheDocument()

  expect(queryByTestId('day-date')).not.toBeInTheDocument()

  expect(getAllByTestId('day-date-loading').length).toBeGreaterThan(0)
  expect(getAllByTestId('day-module-loading').length).toBeGreaterThan(0)
  expect(getAllByTestId('day-time-room-loading').length).toBeGreaterThan(0)
  expect(getAllByTestId('day-professor-loading').length).toBeGreaterThan(0)

  getAllByTestId('day-date-loading').forEach(element => expect(element).toBeInTheDocument())
  getAllByTestId('day-module-loading').forEach(element => expect(element).toBeInTheDocument())
  getAllByTestId('day-time-room-loading').forEach(element => expect(element).toBeInTheDocument())
  getAllByTestId('day-professor-loading').forEach(element => expect(element).toBeInTheDocument())
})

it('Renders error message', async () => {

  const selectedCourse = 'testCourse'

  nock(apiUrl)
    .get(new RegExp(getCourseEventsEndpoint))
    .reply(400)

  const { queryByTestId, getByTestId } = render(CourseView, {selectedCourse})

  await sleep(100)

  expect(queryByTestId('course-view-events-wrapper')).not.toBeInTheDocument()

  expect(queryByTestId('day-date')).not.toBeInTheDocument()

  expect(queryByTestId('day-date-loading')).not.toBeInTheDocument()
  expect(queryByTestId('day-module-loading')).not.toBeInTheDocument()
  expect(queryByTestId('day-time-room-loading')).not.toBeInTheDocument()
  expect(queryByTestId('day-professor-loading')).not.toBeInTheDocument()

  expect(getByTestId('course-view-error-message')).toBeInTheDocument()
})
