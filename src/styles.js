export const standardColorScheme = {
  primaryColor: '#cc1236',
  secondaryColor: '#cc1236',
  thirdiaryColor: '#3c3c43',
  logoColor: '#f4f0ec',
  mutedColor: '#d3d3d9',
  mutedAltColor: '#e9e9ec',
  textColor: '#333333',
  textAltColor: '#f4f0ec',
  backgroundColor: '#f4f0ec',
  backgroundAltColor: '#f4f0ec'
}

export const darkColorScheme = {
  primaryColor: '#8e1934',
  secondaryColor: '#8e1934',
  thirdiaryColor: '#3c3c43',
  logoColor: '#f4f0ec',
  mutedColor: '#555555',
  mutedAltColor: '#666666',
  textColor: '#eeeeee',
  textAltColor: '#eeeeee',
  backgroundColor: '#333333',
  backgroundAltColor: '#444444'
}
