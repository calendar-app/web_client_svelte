import { standardColorScheme } from "./styles"

export function isToday(date) {

  const today = new Date()

  return date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
}

export function isTomorrow(date) {

  const tomorrow = new Date()
  tomorrow.setDate(tomorrow.getDate() + 1)

  return date.getDate() === tomorrow.getDate() &&
    date.getMonth() === tomorrow.getMonth() &&
    date.getFullYear() === tomorrow.getFullYear()
}

export function capitalize(string) {

  return string
    .toLowerCase()
    .replace(/\w\S*/g,
        (w) => (w.replace(/^\w/,
          (c) => c.toUpperCase()
        )
      )
    )
}

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function getMonday(date) {

  const dayOfWeek = date.getDay()
  const diff = date.getDate() - dayOfWeek + (dayOfWeek == 0 ? -6 : 1)

  date.setDate(diff)
  return date
}

export function isHexColorString(str) {
  return /^#[0-9A-F]{6}$/i.test(str) || /^#[0-9A-F]{3}$/i.test(str) || /^#[0-9A-F]{8}$/i.test(str)
}

export function newConfig() {
  return {
    colorScheme: {
      force: false,
      ...standardColorScheme,
    },
    popups: {
      surveyPopup: true
    },
    buttons: {
      openInNewTab: false,
      darkMode: true
    },
    tinyHeader: false,
    courseSelection: true,
    selectedCourse: localStorage.getItem('selectedCourse') ?? ''
  }
}
