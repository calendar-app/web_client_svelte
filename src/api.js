import { getMonday } from './utils'

export const apiUrl = 'https://orari.federicoantoniazzi.dev'
export const getCoursesEndpoint = '/api/courseList'
export const getCourseEventsEndpoint = '/api'

export async function getCourses(){

  const data = await fetch(apiUrl+getCoursesEndpoint)
  return await data.json()
}

export async function getCourseEventsByWeek(course){

  const res = await fetch(`${apiUrl}${getCourseEventsEndpoint}/${course}?startDay=${getMonday(new Date()).toISOString().split('T')[0]}`)

  const content = await res.json()

  let ret = content.map(element => sortByDateAndTime( groupByDate(element.days) ))

  return ret
}

function groupByDate(arr) {

  return arr.reduce((acc, current) => {

    let filtered = {
      // date:       current.date,
      module:     current.module,
      professor:  current.professor,
      timeStart:  current.timeStart,
      timeEnd:    current.timeEnd,
      room:       current.room,
    }

    let date = new Date(current.date.split('T')[0])
    let dateObj = acc.find(element => element.date.getTime() === date.getTime())

    if(dateObj) dateObj.events = [...dateObj.events, filtered]

    else{
      acc.push({
        date,
        events: [filtered],
      })
    }

    return acc

  },[])
}

function sortByDateAndTime(arr) {

  arr.forEach(day => {
    day.events.sort((a,b) => {

      let timeA = parseFloat(a.timeStart)
      let timeB = parseFloat(b.timeStart)

      if(timeA < timeB) return -1
      if(timeA > timeB) return 1

      return 0
    })
  });

  arr.sort((a,b) => {

    if(a.date < b.date) return -1
    if(a.date > b.date) return 1

    return 0
  })

  return arr
}
