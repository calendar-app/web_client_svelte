module.exports = {
  transform: {
    '^.+\\.svelte$': 'svelte-jester',
    '^.+\\.js$': 'babel-jest',
  },
  moduleFileExtensions: ['js', 'svelte'],
  setupFiles: ['<rootDir>/src/tests/setupTests.js'],
  testEnvironment: 'jsdom'
}
