FROM node:lts-alpine AS builder
WORKDIR /usr/src/app
COPY . .
RUN npm ci
# RUN npm clean
RUN npm run test
RUN npm run build

FROM nginx:alpine as release
WORKDIR /usr/share/nginx/html
EXPOSE 80
COPY --from=builder ./usr/src/app/public/ .
COPY nginx.conf /etc/nginx/nginx.conf
